# We need to make sure that python knows about traci
# If you have trouble importing traci, you may need to edit the path in `lib/import_sumo.py`
from lib import import_sumo

# import traci
import traci
import traci.constants as tc

# other dependencies
from time import sleep, time

# Assumes `sumo-gui` is on the path already.  If not, you will need to update it to include the fully qualified path to your sumo-gui binary
sumoBinary = "sumo-gui"
sumoCmd = [sumoBinary, "-c", "simple.sumocfg",
           "--start", "--time-to-teleport", "100000000"]

# Start the simulation
traci.start(sumoCmd)
step = 0


# Begin the simulation.  We set the step time to a high number (10000 seconds) to keep it running for a while
while step < 10000:

    # Get the current epoch time
    t = int(time())

    # Simulate the SUMO step
    traci.simulationStep()

    # Below are some helpful functions that might come in handy
   
    speed1 = traci.vehicle.getSpeed("veh1") # m/s
    current_road1 = traci.vehicle.getRoadID("veh1") # current edge name
    if current_road1 == "main_3_wb":
       traci.vehicle.setStop("veh1", "x1_s_sb", duration = 30)
    #fuel_consumption1 = traci.vehicle.getFuelConsumption("veh1") # ml/s
    
       # For other available functions on `traci.vehicle`, check out https://sumo.dlr.de/pydoc/traci._vehicle.html

    # increase the step for the next iteration
    step += 1

    # slow the simulation down so we can watch it
    sleep(0.2)
